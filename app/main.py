from flask import Flask, request, jsonify
from flask_cors import CORS
import json
import os
import requests
from Levenshtein import ratio
from random import randint

CONSUL_URL = os.getenv('BOT_CONSUL_URL', None)
if not CONSUL_URL:
    raise Exception('BOT_CONSUL_URL env variable is not set!')

DOCKER_URL = CONSUL_URL[:-5]
print(DOCKER_URL)

CONSUL_SERVICES_CATALOG = "services/"
HEADERS = {'Content-type': 'application/json'}
CURRENCY_HELPER_SERVICE = "currency"

CURRENCY_MAP = {}
with open("currency.data", 'r', encoding='utf8') as f:
    for line in f:
        CURRENCY_MAP[line[:3].lower()] = line[4:-1].lower()

app = Flask(__name__)

CORS(app,
    resources={
        r'/*': {
            'origins': '*',
            'headers': ['Content-Type']
        }
    }
)

@app.route('/', methods=['GET'])
def test():
    return "STATUS OK - Consul version " + CONSUL_URL + " " + str(API_SERVICES)


@app.route('/reload', methods=['GET'])
def reload():
    global API_SERVICES
    API_SERVICES = loadServices()
    return jsonify(API_SERVICES)


@app.route('/engine/', methods=['POST'])
def engine():
    response = "Upss... coś poszło nie tak"
    if request.method == "POST":
        message = retrieveMessage(request.get_json(), "message")
        if message:
            res = processMessage(message)
            if res:
                response = res
    
    return jsonify({"response": response})


def loadServices():
    API_SERVICES_NEW = {}
    try:
        response = requests.get(CONSUL_URL + "/v1/kv/" + CONSUL_SERVICES_CATALOG + "?keys", headers=HEADERS)
        services_list = response.json()
    except:
        services_list = []

    for service in services_list:
        service_name = service[len(CONSUL_SERVICES_CATALOG):]
        words = requests.get(CONSUL_URL + "/v1/kv/" + CONSUL_SERVICES_CATALOG + service_name + "?raw", headers=HEADERS)
        API_SERVICES_NEW[service_name] = words.text.split(",")
    return API_SERVICES_NEW


def getServiceURL(service):
    response = requests.get(CONSUL_URL + "/v1/catalog/service/" + service, headers=HEADERS)
    response_json = response.json()
    random_num = randint(0, len(response_json)-1)
    node = response.json()[random_num]
    service_url = DOCKER_URL + ":" + str(node["ServicePort"]) + "/" + service
    print(service_url)
    return service_url


def retrieveMessage(body, keyword):
    if body and isinstance(body, dict):
        for key, value in body.items():
            if key == keyword:
                return value
    return None


def processMessage(message):
    words = message.lower().split()
    
    for service, keywords in API_SERVICES.items():
        for keyword in keywords:
            if keyword in words:
                words.remove(keyword)
                if CURRENCY_HELPER_SERVICE in API_SERVICES and keyword in API_SERVICES[CURRENCY_HELPER_SERVICE]:
                    return run_helper(words, service)
                else:
                    return send(' '.join(words), service)
    return "Upss... coś poszło nie tak"


def send(message, service):
    try:
        data = json.dumps({"data": message})
        response = requests.post(getServiceURL(service), data=data, headers=HEADERS)
        return retrieveMessage(response.json(), "data")
    except:
        return "Upss... coś poszło nie tak"


def run_helper(words, service):
    print(words)
    for word in words:
        if word in CURRENCY_MAP.keys():
            return send(word, service)
    line = ' '.join(words)
    print(line)
    for curr in CURRENCY_MAP.keys():
        if CURRENCY_MAP[curr] in line:
            return send(curr, service)

    currency_matched = {}
    for word in words:
        for curr, text in CURRENCY_MAP.items():
            if word in text.split():
                currency_matched[curr] = text

    if len(currency_matched) > 0:
        tmp = []
        for curr, text in currency_matched.items():
            tmp.append(text + " (" + curr + ")")
        return "Nie znaloziono takej waluty. Kilka podpowiedzi: " + ", ".join(tmp)

    matches = []
    for curr in CURRENCY_MAP.keys():
        matches.append((CURRENCY_MAP[curr] + " (" + curr + ")", ratio(line, CURRENCY_MAP[curr])))
    matches = sorted(matches, key=lambda x: x[1], reverse=True)
    
    tmp = []
    for i in range(0, 5):
        tmp.append(matches[i][0])
    return "Nie znaloziono takej waluty. Kilka podpowiedzi: " + ", ".join(tmp)


API_SERVICES = loadServices()

if __name__ == "__main__":
    app.run(host='localhost', debug=True, port=80)
